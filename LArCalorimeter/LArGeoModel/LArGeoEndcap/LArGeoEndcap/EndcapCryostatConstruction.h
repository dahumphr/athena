/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file EndcapCryostatConstruction.h
 *
 * @brief Declaration of EndcapCryostatConstruction class
 *
 * $Id: EndcapCryostatConstruction.h,v 1.14 2009-02-10 16:50:53 tsulaia Exp $
 */

#ifndef LARGEOENDCAP_ENDCAPCRYOSTATCONSTRUCTION_H
#define LARGEOENDCAP_ENDCAPCRYOSTATCONSTRUCTION_H

#include "GeoModelKernel/GeoPhysVol.h"
#include "GeoModelKernel/GeoFullPhysVol.h"
#include "LArGeoHec/HEC2WheelConstruction.h"
#include "LArGeoFcal/FCALConstruction.h"
#include "LArGeoEndcap/EMECConstruction.h"

class IRDBRecord;
class LArDetectorToolNV;
class StoredMaterialManager;

namespace LArGeo {

  /** @class LArGeo::EndcapCryostatConstruction
      @brief Description of the LAr End Cap cryostat, including MBTS description
   */
  class EndcapCryostatConstruction
    {
    public:

    EndcapCryostatConstruction(
        bool fullGeo,
        std::string emecVariantInner = "Wheel",
        std::string emecVariantOuter = "Wheel",
        bool activateFT = false,
        bool enableMBTS = true
    );
    ~EndcapCryostatConstruction() = default;

    EndcapCryostatConstruction(const EndcapCryostatConstruction&) = delete;
    EndcapCryostatConstruction& operator= (const EndcapCryostatConstruction&) = delete;

    // Get the envelope containing one endcap (pos/neg)
    GeoIntrusivePtr<GeoFullPhysVol>  createEnvelope(bool bPos);

    virtual GeoIntrusivePtr<GeoFullPhysVol> GetEnvelope() { return  GeoIntrusivePtr<GeoFullPhysVol>{};}


    // Set a vis limit for the FCAL
    void setFCALVisLimit(int limit) {m_fcalVisLimit=limit;}

  private:

    int                 m_fcalVisLimit;

    EMECConstruction          m_emec;
    HEC2WheelConstruction     m_hec2;
    FCALConstruction          m_fcal;

    bool                      m_fullGeo;  // true->FULL, false->RECO
    std::string m_EMECVariantInner;
    std::string m_EMECVariantOuter;

    bool m_activateFT;
    bool m_enableMBTS;

    friend class ::LArDetectorToolNV;

    static GeoIntrusivePtr<GeoPhysVol> buildMbtsTrd(const IRDBRecord* rec
			     , StoredMaterialManager* matmanager
			     , GeoIntrusivePtr<GeoPhysVol> parent);
  };

} // namespace LArGeo

#endif // LARGEOENDCAP_ENDCAPCRYOSTATCONSTRUCTION_H
