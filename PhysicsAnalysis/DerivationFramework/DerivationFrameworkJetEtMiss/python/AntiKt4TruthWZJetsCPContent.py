# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

AntiKt4TruthWZJetsCPContent = [
"AntiKt4TruthWZJets",
"AntiKt4TruthWZJetsAux.pt.eta.phi.m.PartonTruthLabelID.HadronConeExclExtendedTruthLabelID.HadronConeExclTruthLabelID.TrueFlavor.GhostBHadronsFinalCount.GhostBHadronsFinal"
]

