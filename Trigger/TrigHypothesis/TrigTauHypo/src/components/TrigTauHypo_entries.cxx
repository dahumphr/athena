#include "../TrigTauCaloHypoAlg.h"
#include "../TrigTauCaloHypoTool.h"

#include "../TrigTauFastTrackHypoAlg.h"
#include "../TrigTauFastTrackHypoTool.h"

#include "../TrigTauPrecTrackHypoAlg.h"
#include "../TrigTauPrecTrackHypoTool.h"

#include "../TrigTauPrecisionHypoAlg.h"
#include "../TrigTauPrecisionIDHypoTool.h"
#include "../TrigTauPrecisionDiKaonHypoTool.h"

DECLARE_COMPONENT( TrigTauCaloHypoAlg )
DECLARE_COMPONENT( TrigTauCaloHypoTool )

DECLARE_COMPONENT( TrigTauFastTrackHypoAlg )
DECLARE_COMPONENT( TrigTauFastTrackHypoTool )

DECLARE_COMPONENT( TrigTauPrecTrackHypoAlg )
DECLARE_COMPONENT( TrigTauPrecTrackHypoTool )

DECLARE_COMPONENT( TrigTauPrecisionHypoAlg )
DECLARE_COMPONENT( TrigTauPrecisionIDHypoTool )
DECLARE_COMPONENT( TrigTauPrecisionDiKaonHypoTool )
